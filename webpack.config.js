const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const PurifyCSSPlugin = require('purifycss-webpack');

module.exports = {
  entry: {
    common: ['bootstrap-loader/extractStyles', './src/main.js'],
    index: './src/assets/js/index.js',
    contact: './src/assets/js/contact.js',
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9001,
    noInfo: false,
    stats: {
      errors: true,
      errorDetails: true,
      colors: true
    }
  },
  module: {
    rules: [
      // SASS Rule
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          //resolve-url-loader may be chained before sass-loader if necessary
          use: ['css-loader', 'sass-loader']
        })
      },
      // Font Rule
      {
         test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
         use: ['file-loader']
      },
      // Image Rule
      {
        test: /\.(png|jpe?g|svg)$/i,
        use: [
          'file-loader?name=[name].[ext]&outputPath=assets/img/',
          'image-webpack-loader'
        ]
      },
      // Bootstrap 3
      {
        test: /bootstrap-sass\/assets\/javascripts\//,
        use:  'imports-loader?jQuery=jquery'
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {"watch": true}),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common' // Specify the common bundle's name.
    }),
    new HtmlWebpackPlugin({
      title: '_Template | Home',
      minify: {
        collapseWhitespace: false,
        removeComments: true
      },
      hash: true,
      filename: 'index.html',
      excludeChunks: ['contact', 'main'],
      template: './src/templates/index.ejs',
    }),
    new HtmlWebpackPlugin({
      title: '_Template | Contact Us',
      minify: {
        collapseWhitespace: false,
        removeComments: true
      },
      hash: true,
      filename: 'contact.html',
      excludeChunks: ['index', 'main'],
      // chunks: ['bootstrap', 'contact'],
      template: './src/templates/contact.ejs'
    }),
    new ExtractTextPlugin({
      filename: "./assets/css/[name].css"
    }),
    new PurifyCSSPlugin({
      // Give paths to parse for rules. These should be absolute!
      paths: glob.sync(path.join(__dirname, 'src/templates/*.ejs')),
    })
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    chunkFilename: '[name].bundle.js',
    filename: './assets/js/[name].bundle.js'
  }
}
