# Simple Starter Project
### with webpack.js and npm (assets bundler & package manager respectively).
***

## NPM.JS
#### <https://www.npmjs.com/>
#### [Doc Page](https://docs.npmjs.com/)

## WebPack.JS
#### <https://webpack.js.org/>
#### [Doc Page](https://webpack.js.org/configuration/)

## Petr Tichy
#### [Video Tutorial on webpack.js](https://www.youtube.com/playlist?list=PLkEZWD8wbltnRp6nRR8kv97RbpcUdNawY)




![Minion](http://octodex.github.com/images/minion.png)
